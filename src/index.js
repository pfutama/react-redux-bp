import React from "react";
import ReactDOM from "react-dom";

import "Styles/Main.scss";
import App from "./App";

import {createStore } from "redux";
import { Provider } from "react-redux";

const initialState = {
  favPokemon: [],
}

const rootReducer = ( state = initialState, action) => {
  switch (action.type) {
    case "SET_NEW_POKEMON":
      return {
        ...state,
        favPokemon: state.favPokemon.concat(action.newValue),
      };
  
    default:
      break;
  }
  return state;
}

const storeRedux = createStore(rootReducer);

ReactDOM.render(
  <React.StrictMode>
    <Provider store = {storeRedux}>
    <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
